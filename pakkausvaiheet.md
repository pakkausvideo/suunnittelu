Pakkauksen eri vaiheet:

1. Valmistelu
	1. Varjon laskeminen maahan
	1. Taaksekävely
	1. Repun riisuminen
	1. Repun laskeminen maahan
	1. Korkeusmittarin vienti
	1. Ota paranaru
	1. Ota varmistuslanka
	1. Ota kansio
	1. Ota kynä
	1. Puhdista reppu
	1. Tarkista looppi
	1. Laita paranaru

1. Sormet sisään ja ravistus

1. Kierteiden poisto

1. Jarrujen laitto

1. Punosten selvitys
	1. Sormet sisään ja kävely
	1. Ravistus
	1. Ohjauspunosten etsiminen

1. Tarkastus

1. Maahan laitto

1. Podiin laitto

1. Punostus

1. Sulkeminen (pinni kiinni)

1. Laukaisujärjestelmän laitto

1. Viimeistely


