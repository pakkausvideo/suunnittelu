# Pakkausvideo #

Tarkoituksena valmistaa joukkoistettuna pakkausvideo.

Tyylilajina on Ylvis. Ideaa toteutusmalliksi: https://www.youtube.com/watch?v=TUHgGK-tImY
Muiden Ylvis-pätkien katsominen suositeltavaa (TJEU Stonehenge, Massachusetts).

Tämä repository on tarkoitettu suunnitteluun. Dokumentit kirjoitetaan markdownia käyttäen.

Ensimmäinen vaihe on pakkausvaiheiden selvittäminen, jonka jälkeen voidaan lähteä suunnittelemaan mahdollista sanoitusta.
